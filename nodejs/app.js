//Imports
var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var app = express();

// This sets up the view engine which can be used to render html pages from the jade language
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

//Set up other components
//The Request logger. Outputs requests to the console
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());


//Setup the directory route for static content.
app.use(express.static('../public'));
app.use('/', require('./routes/index'));
app.use('/tutorscout24/api/v1/',require('./routes/tutorscout24/api/v1/apiIndex'));
app.use('/tutorscout24/api/v1/user',require('./routes/tutorscout24/api/v1/userApi/userApiRouter'));
app.use('/tutorscout24/api/v1/tutoring',require('./routes/tutorscout24/api/v1/tutoringsApi/tutoringApiRouter'));
app.use('/tutorscout24/api/v1/message',require('./routes/tutorscout24/api/v1/messageApi/messageApiRouter'));
//app.use('/tutorscout24/api/v1/tutoring',require('./routes/tutorscout24/api/v1/imageApi/imageApiRouter'));
app.use('/tutorscout24/api/v1/possibleTime',require('./routes/tutorscout24/api/v1/possibleTimeApi/possibleTimeApiRouter'));
app.use('/tutorscout24/api/v1/tag',require('./routes/tutorscout24/api/v1/tagApi/tagApiRouter'));


// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  console.error(err.toString());
  res.status(err.status || 500).send(JSON.stringify(err.message, null, 4));
});
console.log("Error handlers successfully set up");

module.exports = app;
