function TutoringFindRequest(latitude,longitude,rangeKm,rowLimit,rowOffset)
{
    this.latitude=latitude;
    this.longitude=longitude;
    this.rowLimit=rowLimit;
    this.rowOffset=rowOffset;
    this.rangeKm = rangeKm;
}

var TutoringFindRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "latitude": {"type": "double"},
        "longitude": {"type": "double"},
        "rangeKm": {"type": "double"},
        "rowLimit": {"type": "integer"},
        "rowOffset": {"type": "integer"},
    },
    "required": ["latitude","longitude","rangeKm","rowLimit","rowOffset"]
};

module.exports.TutoringFindRequest=TutoringFindRequest;
module.exports.Schema =TutoringFindRequestSchema;

