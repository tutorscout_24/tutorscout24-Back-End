function messagesGet() {
    this.userId = "";
}
var messagesGetSchema = {
    "id":"/messageGet",
    "type":"object",
    "properties":{
        "userId":{"type":"string"}
    },
    "required":["userId"]
};

module.exports.messagesGet = messagesGet;
module.exports.Schema = messagesGetSchema;