function PossibleTimeGetRequest(tutoringId,userId,start,end)
{
    this.tutoringId=tutoringId;
}

var PossibleTimeGetRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tutoringId": {"type": "integer"},
    },
    "required": ["tutoringId"]
};

module.exports.TutoringFindRequest=PossibleTimeGetRequest;
module.exports.Schema =PossibleTimeGetRequestSchema;

