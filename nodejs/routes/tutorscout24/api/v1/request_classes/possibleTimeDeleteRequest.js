function PossibleTimeDeleteRequest(tutoringId,userId,start,end)
{
    this.tutoringId=tutoringId;
}

var PossibleTimeDeleteRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "timeId": {"type": "integer"},
    },
    "required": ["timeId"]
};

module.exports.TutoringFindRequest=PossibleTimeDeleteRequest;
module.exports.Schema =PossibleTimeDeleteRequestSchema;

