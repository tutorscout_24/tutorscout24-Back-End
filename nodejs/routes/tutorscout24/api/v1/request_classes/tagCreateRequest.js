function TagCreateRequest(tutoringId,tagText)
{
    this.tagText=tagText;
    this.tutoringId=tutoringId;
}

var TagCreateRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tutoringId": {"type": "integer"},
        "tagText": {"type": "string"},
    },
    "required": ["tutoringId","tagText"]
};

module.exports.TagCreateRequest=TagCreateRequest;
module.exports.Schema =TagCreateRequestSchema;

