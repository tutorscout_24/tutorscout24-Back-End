function TagGetTutoringTags(tutoringId)
{
    this.tutoringId=tutoringId;
}

var TagGetTutoringTagsRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tutoringId": {"type": "integer"},
    },
    "required": ["tutoringId"]
};

module.exports.TagGetTutoringTags=TagGetTutoringTags;
module.exports.Schema =TagGetTutoringTagsRequestSchema;

