function TagDeleteRequest(tagId)
{
    this.tagText=tagId;
}

var TagDeleteRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tagId": {"type": "integer"},
    },
    "required": ["tagId"]
};

module.exports.TagDeleteRequest=TagDeleteRequest;
module.exports.Schema =TagDeleteRequestSchema;