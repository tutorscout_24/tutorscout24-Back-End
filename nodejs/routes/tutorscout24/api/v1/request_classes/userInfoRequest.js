var userInfoRequest = {
    "id": "/userInfoRequest",
    "type": "object",
    "properties": {
        "userToFind": {"type": "string","pattern":"^[a-zA-Z0-9]*$","minLength":1}
    },
    "required": ["userToFind"]
};

module.exports.Schema = userInfoRequest;