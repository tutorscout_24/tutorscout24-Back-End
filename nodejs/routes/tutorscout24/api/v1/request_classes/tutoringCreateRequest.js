function Tutoring() {
    this.subject = "";
    this.text = "";
    this.duration=0;
    this.latitude = 0.0;
    this.longitude = 0.0;
}

var tutoringSchema = {
    "id": "/Tutoring",
    "type": "object",
    "properties": {
        "subject": {"type": "string","minLength":1,"maxLength":45},
        "text": {"type": "string","minLength":1,"maxLength":1000},
        "duration": {"type": "int"},
        "latitude": {"type": "double"},
        "longitude": {"type": "double"},
    },
    "required": [
        "subject",
        "text",
        "duration",
        "latitude",
        "longitude"]
};


module.exports.User = Tutoring;
module.exports.Schema = tutoringSchema;