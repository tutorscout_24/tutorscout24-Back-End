function messageSend() {
    this.toUserId ="";
    this.text="";

}

var messageSendSchema = {
    "id":"/messageSend",
    "type": "object",
    "properties":{
        "toUserId": {"type":"string"},
        "text": {"type": "string", "maxLength":500}
    },
    "required":[ "toUserId", "text"]
};
module.exports.messageSend = messageSend;
module.exports.Schema = messageSendSchema;