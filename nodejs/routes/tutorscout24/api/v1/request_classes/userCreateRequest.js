emailregex= require("../data_classes/regex").emailregex;
passwordregex= require("../data_classes/regex").passwordregex;

function User() {
    this.userName = "";
    this.password = "";
    this.firstName = "";
    this.lastName = "";
    this.birthdate = "";
    this.gender = "";
    this.email = "";
    this.note = "";
    this.placeOfResidence = "";
    this.maxGraduation = "";
}

var userSchema = {
    "id": "/User",
    "type": "object",
    "properties": {
        "userName": {"type": "string","pattern":"^[a-zA-Z0-9]*$","minLength":3},
        "firstName": {"type": "string","pattern":"^[a-zA-Z0-9]*$","minLength":2},
        "lastName": {"type": "string","pattern":"^[a-zA-Z0-9]*$","minLength":2},
        "birthdate": {"type": "string", "pattern":"^[0-9]*$", "maxLength":8,"minLength":8},
        "gender": {"type": "string","pattern":"^[a-zA-Z0-9]*$","minLength":4},
        "email": {"type": "string","pattern": emailregex},
        "note": {"type": "string"},
        "placeOfResidence": {"type": "string"},
        "maxGraduation": {"type": "string"},
        "password": {"type": "string","pattern":passwordregex}
    },
    "required": ["userName",
        "firstName",
        "lastName",
        "birthdate",
        "gender",
        "email",
        "note",
        "placeOfResidence",
        "maxGraduation",
        "password"]
};


module.exports.User = User;
module.exports.Schema = userSchema;