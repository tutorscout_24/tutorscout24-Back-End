var dateTimeRegex = require(".././data_classes/regex").datetimeregex

function PossibleTimeCreateRequest(tutoringId,userId,start,end)
{
    this.start=start;
    this.end=end;
    this.tutoringId=tutoringId;
    this.userId=userId;
}

var PossibleTimeCreateRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tutoringId": {"type": "integer"},
        "start": {"type": "double","pattern":dateTimeRegex},
        "end": {"type": "double","pattern":dateTimeRegex},
    },
    "required": ["tutoringId","start","end"]
};

module.exports.TutoringFindRequest=PossibleTimeCreateRequest;
module.exports.Schema =PossibleTimeCreateRequestSchema;

