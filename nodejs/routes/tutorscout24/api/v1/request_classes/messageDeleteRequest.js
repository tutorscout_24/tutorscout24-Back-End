function messageDelete() {
    this.messageId = "";
    this.userId = "";
}
var messageDeleteSchema = {
    "id":"/messageDelete",
    "type": "object",
    "properties":{
        "messageId": {"type":"integer"}
    },
    "required":["messageId"]
};

module.exports.messageDelete = messageDelete;
module.exports.Schema = messageDeleteSchema;