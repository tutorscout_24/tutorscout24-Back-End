emailregex= require("../data_classes/regex").emailregex;
passwordregex= require("../data_classes/regex").passwordregex;

function UserUpdate() {
    this.password = "";
    this.firstName = "";
    this.lastName = "";
    this.birthdate = "";
    this.gender = "";
    this.email = "";
    this.note = "";
    this.placeOfResidence = "";
    this.maxGraduation = "";
}

var userUpdateSchema = {
    "id": "/User",
    "type": "object",
    "properties": {
        "firstName": {"type": "string","pattern":"^[a-zA-Z0-9]*$"},
        "lastName": {"type": "string","pattern":"^[a-zA-Z0-9]*$"},
        "birthdate": {"type": "string","pattern":"^[0-9]*$", "maxLength":8, "minLength":8},
        "gender": {"type": "string","pattern":"^[a-zA-Z0-9]*$"},
        "email": {"type": "string","pattern": emailregex},
        "note": {"type": "string"},
        "placeOfResidence": {"type": "string"},
        "maxGraduation": {"type": "string"},
        "password": {"type": "string","pattern":passwordregex}
    }
};


module.exports.User = UserUpdate;
module.exports.Schema = userUpdateSchema;