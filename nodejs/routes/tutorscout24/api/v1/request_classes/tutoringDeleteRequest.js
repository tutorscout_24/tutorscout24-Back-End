function TutoringDeleteRequest(userId,tutoringId)
{
    this.userId=userId;
    this.tutoringId=tutoringId;
}

var TutoringDeleteRequestSchema= {
    "id": "/TutoringDeleteRequest",
    "type": "object",
    "properties": {
        "tutoringId": {"type": "integer"},
    },
    "required": ["tutoringId"]
};

module.exports.TutoringDeleteRequest=TutoringDeleteRequest;
module.exports.Schema =TutoringDeleteRequestSchema;

