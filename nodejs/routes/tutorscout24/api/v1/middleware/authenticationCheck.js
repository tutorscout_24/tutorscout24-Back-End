/*A middleware that checks if a request contains a valid authentication. Can be added before all router routes that requeue user authentication.*/

var userDb = require('../database/users_database');
var Authentication = require('../data_classes/authentication');
var validate = require('jsonschema').validate;


function checkAuthentication(req, res, next) {

    //Deni the request because the Authentication is missing.
    if (!req.body.hasOwnProperty('authentication')) {
        res.status(400).json('Authentication is missing');
        return;
    }

    //Deni the request because the Authentication has the wrong format.
    var validation = validate(req.body.authentication, Authentication.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }

    //Check if the given password equals the one stored in the database.
    userDb.checkPassword(req.body.authentication.userName, req.body.authentication.password, function (error, successful) {
        if (error) {
            res.status(500).json("Error during database query: "+error);
            console.log(error.message);
            return;
        }

        //Continue to the next handler if the Authentication is valid.
        if (successful) {
            next();
        }
        else {
            res.status(403).json('Authentication is invalid')
        }
    })
}

module.exports = checkAuthentication;