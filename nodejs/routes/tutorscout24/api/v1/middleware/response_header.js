/*A middleware that prepares the header data of all api responses*/

function setHeader(req, res, next) {
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');
    res.type('application/json');
    next();
}

module.exports = setHeader;