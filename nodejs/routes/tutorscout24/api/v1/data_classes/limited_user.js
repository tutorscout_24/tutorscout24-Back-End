/*Class for the response of the general user info request*/
function LimitedUser(userid, firstName, lastName, age, gender, description, maxgraduation)
{
    this.userid= userid;
    this.firstName= firstName;
    this.lastName= lastName;
    this.age= age;
    this.gender= gender;
    this.description= description;
    this.maxGraduation = maxgraduation;
}
module.exports.LimitedUser=LimitedUser;