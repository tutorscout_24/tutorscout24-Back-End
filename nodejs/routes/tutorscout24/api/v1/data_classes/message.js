/* Class for the response of all get messages requests*/
function Message(messageId,datetime,fromUserId,toUserId,text) {
    this.messageId = messageId;
    this.datetime = datetime;
    this.fromUserId = fromUserId;
    this.toUserId = toUserId;
    this.text = text;

}
module.exports.Message = Message;