passwordregex= require("./regex").passwordregex;


/*General class for authentication data*/
function Authentication(username,password)
{
    this.userName=username;
    this.password=password;
}


/*Schema for validating a request authentication with the jsonschema module*/
var AuthenticationSchema= {
    "id": "/Authentication",
    "type": "object",
    "properties": {
        "userName": {"type": "string" ,"minLength": 3,"pattern":"^[a-zA-Z0-9]*$"},
        "password": {"type": "string","pattern":passwordregex}
    },
    "required": ["userName",
        "password"]
};

module.exports.Authentication=Authentication;
module.exports.Schema =AuthenticationSchema;

