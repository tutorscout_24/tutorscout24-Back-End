/*Data class for the myUserInfo request. A request that returns the full user information of the current user*/
function MyUserInfo(userid, firstName, lastName, dayOfBirth, gender, description, maxgraduation,email,placeOfResidence)
{
    this.userid= userid;
    this.firstName= firstName;
    this.lastName= lastName;
    this.dayOfBirth= dayOfBirth;
    this.gender= gender;
    this.description= description;
    this.maxGraduation = maxgraduation;
    this.email=email;
    this.placeOfResidence=placeOfResidence;
}
module.exports.MyUserInfo=MyUserInfo;