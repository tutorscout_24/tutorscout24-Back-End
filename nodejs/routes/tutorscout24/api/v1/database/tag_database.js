/*Contains all operations on the database that belong to the 'tag' database table.*/

var connection = require('./database_connection').Connection;

function createTutoringTag(tutoringId,tagText,callback)
{
    var sql ="INSERT INTO `tutorscout`.`tutoringtag` (`tutoringid`, `text`) VALUES ("+connection.escape(tutoringId)+", "+connection.escape(tagText)+");";
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error);

        return callback(error);
    });
}


function getTutoringTags(tutoringId,callback)
{
    var sql ="SELECT tagid as tagId,tutoringid as tutoringId,text FROM tutorscout.tutoringtag where tutoringid = "+connection.escape(tutoringId)+";"
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,results);
    });
}

function deleteTutoringTag(tagId,callback)
{
    var sql ="DELETE FROM tutorscout.tutoringtag where tagid = "+connection.escape(tagId)+";"
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,results.affectedRows >0);
    });
}

//Export of the functions defined in this module
module.exports.createTutoringTag = createTutoringTag;
module.exports.getTutoringTags = getTutoringTags;
module.exports.deleteTutoringTag = deleteTutoringTag;