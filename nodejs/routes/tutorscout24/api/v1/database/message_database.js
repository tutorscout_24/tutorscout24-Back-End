/*Contains all operations on the database that belong to the message database table.*/

//Imports
var express = require('express');
var connection = require('./database_connection').Connection;
var mysql = require('mysql');



/*Stores a message in the Database table message
params:
        message: object of the messageSendRequest class
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.
 */
function storeMessage(message, callback) {
    var query = "INSERT INTO message (unread, fromuserid, touserid, text, deletedbysender, deletedbyreceiver)"
        + "VALUES (" +
        connection.escape(true) + "," +
        connection.escape(message.authentication.userName) + "," +
        connection.escape(message.toUserId) + "," +
        connection.escape(message.text) + "," +
        connection.escape(0) + "," +
        connection.escape(0) + ")";

    connection.query(query, function (error) {

        if(error){
            return callback(error);
        }
        callback(null);
    });
}
/*Check whether the user is the sender or receiver of the message.
params:
        messageToDelete: ID of the message to be checked
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.
                  result is either sender or receiver.
 */
function isUserSenderOrReceiver(messageToDelete, callback) {
    var query = "SELECT fromuserid, touserid FROM message WHERE messageid = " + connection.escape(messageToDelete.messageId);
    connection.query(query, function (error, result) {
        if (error){
            return callback(error, false);
        }
        if (result[0].fromuserid == messageToDelete.authentication.userName){
             return callback(error, "sender");
        }
        if (result[0].touserid == messageToDelete.authentication.userName){
            return callback(error, "receiver");
        }
        else {
            return callback(error, false);
        }

    })
}

/*Delete the message on request from sender
params:
        messageToDelete: ID of the message to be deleted
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function deleteBySender(messageToDelete, callback) {
    var query =  "UPDATE message SET deletedbysender = true WHERE messageid =" + connection.escape(messageToDelete);
    connection.query(query, function (error, result) {
        if (error){
            return callback(error, false);
        }
        else {
            return callback(error, result);
        }
    })
}

/*Delete the message on request from receiver
params:
        messageToDelete: ID of the message to be deleted
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function deleteByReceiver(messageToDelete, callback) {
    var query =  "UPDATE message SET deletedbyreceiver = true WHERE messageid =" + connection.escape(messageToDelete);
    connection.query(query, function (error, result) {
        if (error){
            return callback(error, false);
        }
        else {
            return callback(error, result);
        }
    })
}

/*Remove message from the database if the receiver and sender have deleted it
params:
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function removeMessage(callback) {
    var query = "DELETE FROM message WHERE deletedbyreceiver = true AND deletedbysender = true";
    connection.query(query, function (error, result) {
        if (error) {
            return callback(error, false);
        }
        else {
            return callback(error, result);
        }
    })
}

/*Get all messages the user sent
params:
        user: username of the user who requests the messages
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function getSentMessage(user, callback) {
    var query = "SELECT messageid as messageId,datetime,fromuserid as fromUserId,touserid as toUserId,text FROM message WHERE fromuserid =" + connection.escape(user) + "AND deletedbysender  = false ORDER BY datetime";
    connection.query(query, function (error, result) {
        if (error) {
            return callback(error, false);
        }
        else {
            return callback(error, result);
        }
    })
}

/*Get all messages the user received
params:
        user: username of the user who requests the messages
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function getReceivedMessage(user, callback) {
    var query = "SELECT messageid as messageId,datetime,fromuserid as fromUserId,touserid as toUserId,text FROM message WHERE touserid =" + connection.escape(user) + "AND deletedbyreceiver = false ORDER BY datetime";
    connection.query(query, function (error, result) {
        if (error) {
            return callback(error, false);
        }
        else {
            return callback(error, result);
        }
    })
}

/*Get all received and not read messages
params:
        user: username of the user who requests the messages
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function getUnreadMessage(user, callback) {
    var query = "SELECT messageid as messageId,datetime,fromuserid as fromUserId,touserid as toUserId,text FROM message WHERE touserid =" + connection.escape(user) + " AND deletedbyreceiver = false AND unread = true" + " ORDER BY datetime";
    connection.query(query, function (error, result) {
        if (error) {
            return callback(error, false);
        }
        else {
            return callback(error, result);

        }
    })
}

/*Set all messages to read
params:
        user: username of the user who read his new messages
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.

 */
function messageIsRead(user, callback) {
    var query = "UPDATE message SET unread = false  WHERE touserid = "+ connection.escape(user) +  " AND unread = true";
    connection.query(query, function (error) {
        if (error) {
            return callback(error);
        }
    });

}
module.exports.messageIsRead = messageIsRead;
module.exports.storeMessage = storeMessage;
module.exports.isUserSenderOrReceiver = isUserSenderOrReceiver;
module.exports.deleteBySender = deleteBySender;
module.exports.deleteByReceiver = deleteByReceiver;
module.exports.removeMessage = removeMessage;
module.exports.getSentMessage = getSentMessage;
module.exports.getReceivedMessage = getReceivedMessage;
module.exports.getUnreadMessage = getUnreadMessage;