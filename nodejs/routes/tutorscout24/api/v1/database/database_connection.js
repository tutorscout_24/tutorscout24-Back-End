/*Module that creates an exports the connection to the mysql database. This is for use in all table specific database classes*/

// Module dependencies
var express    = require('express');
var    mysql      = require('mysql');

//Initialization of the database connection.
console.log("Initializing connection to the local sql server");
var connection = mysql.createConnection({
    host     : 'localhost',
    user     : 'root',
    password : 'databasepwd',
    database : 'tutorscout'
});

module.exports.Connection=connection;