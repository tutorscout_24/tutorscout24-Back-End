/*Contains all operations on the database that belong to the 'tutoring' database table.*/

var connection = require('./database_connection').Connection;


function createTutoring(type, userid, tutoring, callback) {

    var expiringData = new Date();
    expiringData.setDate(expiringData.getDate() + tutoring.duration);

    var sql = "INSERT INTO `tutorscout`.`tutoring` (`createruserid`, `subject`, `text`, `type`, `end`, `latitude`, `longitude`) VALUES (" +
        connection.escape(userid) + "," +
        connection.escape(tutoring.subject) + "," +
        connection.escape(tutoring.text) + "," +
        connection.escape(type) + "," +
        connection.escape(expiringData.toISOString().slice(0, 19).replace('T', ' ')) + "," +
        connection.escape(tutoring.latitude) + "," +
        connection.escape(tutoring.longitude) + ");";

    connection.query(sql, function (error, results, fields) {
        if (error) {
            console.error(error);
            return callback(error);
        }

        callback(error);
    });
}

function getUserTutorings(userid, type, callback) {
    var sql = "SELECT * FROM tutorscout.tutoring WHERE type = " + connection.escape(type) + " AND createruserid=" + connection.escape(userid) + ";";

    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error, null);

        callback(error, results);
    });
}


function hasUserTutoring(userid, tutoringid, callback) {
    var sql = "SELECT COUNT(tutoringid)  AS 'count' FROM tutorscout.tutoring WHERE tutoringid = " + connection.escape(tutoringid) + " AND createruserid=" + connection.escape(userid) + ";";

    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error, false);

        return callback(error, results[0].count > 0);
    });
}

function deleteTutoring(tutoringid, callback) {
    var sql = "DELETE FROM `tutorscout`.`tutoring` WHERE `tutoringid`=" + connection.escape(tutoringid) + ";";

    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error);

        return callback(error);
    });
}

function skipOkPacket(arrary) {
    for(var i =0;i<arrary.length;i++)
    {
        if(Array.isArray(arrary[i]))
        {
            return arrary[i];
        }
    }
    return [];
}

function getTutoringOffersWithinRange(latitude,longitude,rangeKm,rowLimit,rowOffset,userid, callback) {
    var sql = "CALL geodistTutoringOffersFromOrigin("+latitude+","+longitude+","+rangeKm+","+rowLimit+","+rowOffset+","+connection.escape(userid.toString())+");";

    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,skipOkPacket(results));
    });
}

function getTutoringRequestsWithinRange(latitude,longitude,rangeKm,rowLimit,rowOffset, userid,callback) {
    var sql = "CALL geodistTutoringRequestsFromOrigin("+latitude+","+longitude+","+rangeKm+","+rowLimit+","+rowOffset+","+connection.escape(userid.toString())+");";

    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,skipOkPacket(results));
    });
}



function createTutoringOffer(userid, tutoring, callback) {
    createTutoring("offer", userid, tutoring, callback);
}

function createTutoringRequest(userid, tutoring, callback) {
    createTutoring("request", userid, tutoring, callback);
}

function getUserTutoringRequests(userid, callback) {
    getUserTutorings(userid, "request", callback);
}

function getUserTutoringOffers(userid, callback) {
    getUserTutorings(userid, "offer", callback);
}


//Export of the functions defined in this module
module.exports.createTutoringOffer = createTutoringOffer;
module.exports.createTutoringRequest = createTutoringRequest;
module.exports.getUserTutoringsRequests = getUserTutoringRequests;
module.exports.getUserTutoringsOffers = getUserTutoringOffers;
module.exports.hasUserTutoring = hasUserTutoring;
module.exports.deleteTutoring = deleteTutoring;
module.exports.getTutoringOffersWithinRange = getTutoringOffersWithinRange;
module.exports.getTutoringRequestsWithinRange = getTutoringRequestsWithinRange;
