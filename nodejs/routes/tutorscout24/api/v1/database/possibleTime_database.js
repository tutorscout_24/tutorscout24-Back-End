/*Contains all operations on the database that belong to the possibletime database table.*/

var connection = require('./database_connection').Connection;


/*Creates a new possible time entry for a given tutoring
params:
        tutoringId: id of the tutoring for which a new possible time entry should be created
        userId: userid(user name) of the user that has created the entry
        start: date time string of the start time of the new possible time entry.
        end: date time string of the end time of the new possible time entry.
        callback: function(error) callback-function that informs the listener about the success or errors that have occured while executing the database operation.
 */
function createPossibleTime(tutoringId,userId,start,end,callback)
{

var sql ="INSERT INTO `tutorscout`.`possibletime` (`start`, `end`, `User_userid`, `tutoringid`) VALUES ("+connection.escape(start)+", "+connection.escape(end)+", "+connection.escape(userId)+", "+connection.escape(tutoringId)+");";
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error);

        return callback(error);
    });
}

/*Gets all possible time entries for a given tutoringId
params:
        tutoringId: id of the tutoring for which all possible time entries should be returned.
        callback: function(error,resultsArray) callback-function that informs the listener about the success or errors that have occured while executing the database operation.
                                               The second parameter of the callback will contain the results of the request(possible time entries.)
 */
function getPossibleTimes(tutoringId,callback)
{
    var sql ="SELECT timeid as timeId,User_userid as userId,start,end FROM tutorscout.possibletime where tutoringid = "+connection.escape(tutoringId)+";"
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,results);
    });
}

/*Deletes a time entry with a given id that was created by a given user.
params:
        timeId: id of the possible time entry that should be created.
        userId: id(user name) of the user that has created the entry.
        callback: function(error,successful) callback-function that informs the listener about the success or errors that have occured while executing the database operation.
 */
function deletePossibleTimes(timeId,userId,callback)
{
    var sql ="DELETE FROM tutorscout.possibletime where timeid = "+connection.escape(timeId)+" AND User_userid="+connection.escape(userId)+";"
    connection.query(sql, function (error, results, fields) {
        if (error) return callback(error,null);

        return callback(error,results.affectedRows >0);
    });
}


//Export of the functions that were defined in this module
module.exports.createPossibleTime = createPossibleTime;
module.exports.getPossibleTimes = getPossibleTimes;
module.exports.deletePossibleTimes = deletePossibleTimes;