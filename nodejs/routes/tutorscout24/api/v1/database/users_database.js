/*Contains all operations on the database that belong to the 'user' database table.*/

var connection = require('./database_connection').Connection;
var bcrypt = require('bcrypt');

/*Get the number of registered users
params:
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function getUserCount(callback) {
    connection.query('SELECT COUNT(userid) AS userCount FROM user', function (error, results, fields) {
        if (error) return callback(error);

        callback(error, results[0].userCount);
    });
}

/*Get user information
params:
        userid: username of requested user
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function userInfo( userid, callback) {
    var query = "SELECT * FROM user WHERE userid =" + connection.escape(userid);
    connection.query(query, function (error, result) {
        if (error){
            return callback (error);
        }
        if(result.length > 1){
            return callback (error);
        }
        else {
            error =  false;
            callback(error, result);
        }
    })
}

/*Check if user exists
params:
        userid: username of requested user
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function doesUserExist(userid, callback) {
    connection.query('SELECT COUNT(userid) AS count FROM user WHERE userid = ' + connection.escape(userid), function (error, results, fields) {
        if (error) return callback(error, false);

        callback(error, results[0].count > 0);
    });
}

/*Check if email exists
params:
        email: email of requested user
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function doesEmailExist(email, callback) {
    connection.query('SELECT COUNT(email) AS count FROM user WHERE email = ' + connection.escape(email), function (error, results, fields) {
        if (error) return callback(error, false);

        callback(error, results[0].count > 0);
    });
}

/*Create a new user
params:
        user: user object
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function createUser(user, callback) {

    bcrypt.hash(user.password, 10, function (err, hash) {

        //Something went wrong while hashing the password
        if (err) {
            callback(error);
        }

        //Build the insert query string. Which is btw a bit long...
        //Escape all data to prevent sql injection(does this library have some hidden prepared statement? could not find any)
        var query = "INSERT INTO user (userid,firstname,lastname,email,password,placeofresidence,birthdate,gender,maxgraduation,description)" +
            " VALUES (" +
            connection.escape(user.userName) + "," +
            connection.escape(user.firstName) + "," +
            connection.escape(user.lastName) + "," +
            connection.escape(user.email) + "," +
            "'" + hash + "'," +
            connection.escape(user.placeOfResidence) + "," +
            connection.escape(user.birthdate) + " ," +
            connection.escape(user.gender) + "," +
            connection.escape(user.maxGraduation) + "," +
            connection.escape(user.note) + ")";

        connection.query(query, function (error, results, fields) {

            //Something went wrong while creating the new user entry in the database.
            if (error) {
                return callback(error);
            }

            callback(null);
        });
    });
}

/*Delete a user
NOT IN USE
params:
        username: username of the user to be deleted
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function checkPassword(userid, password, callback) {
    connection.query('SELECT userid,password FROM user WHERE userid = ' + connection.escape(userid), function (error, results, fields) {

        //General database error
        if (error) {
            return callback(error, false);
        }

        //No db record with was found for the given user(or more than one was found which is also an error that should never happen).
        if (results.length !== 1) {
            return callback(error, false);
        }

        //Compare the given password with the hash from the database
        bcrypt.compare(password, results[0].password, function (err, res) {
            //Res is a bool with the comparison result(true -> successfull)
            callback(error, res);
        });
    });
}

/*Update user information
params:
        user: user object filled with the new information
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function updateUser(user, callback) {

    if ('password' in user) {
        bcrypt.hash(user.password, 10, function (err, hash) {

            if (err) {
                return callback(err);
            }

            user.password = hash;
            //Generate the SQL query string
            var sql = createUserUpdateSqlString(user);


            connection.query(sql, function (error, results, fields) {
                //General database error
                if (error) {
                    callback(error, false);
                    return;
                }
                callback(error, true);
                return;
            });
        });
    }
    else {
       try{
           //Generate the SQL query string
           var sql = createUserUpdateSqlString(user)
           connection.query(sql, function (error, results, fields) {
               //General database error
               if (error) {
                    callback(error, false);
                    return;
               }
               callback(error, true);
               return;
           });
       }
       catch (e) {
           callback(e, false);
           return;
       }
    }
}

/*Generate the userUpdate SQL query string
params:
        user: user object filled with the new information
        callback: function(error) callback-function that informs the listener about the success or errors that have occurred while executing the database operation.

 */
function createUserUpdateSqlString(user) {
    var isFirst = true;
    var appendUpdate = function (sql, key, value, isFirst) {
        if (isFirst) {
            return sql + "`" + key + "` = " + value;
        }
        else {
            return sql + ", `" + key + "` = " + value;
        }
    };


    var sql = "UPDATE `tutorscout`.`user` SET ";
    if (user.hasOwnProperty("firstName")) {
        sql = appendUpdate(sql, "firstname", connection.escape(user.firstName), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("lastName")) {
        sql = appendUpdate(sql, "lastname", connection.escape(user.lastName), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("birthdate")) {
        sql = appendUpdate(sql, "birthdate", connection.escape(user.birthdate), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("gender")) {
        sql = appendUpdate(sql, "gender", connection.escape(user.gender), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("email")) {
        sql = appendUpdate(sql, "email", connection.escape(user.email), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("note")) {
        sql = appendUpdate(sql, "description", connection.escape(user.note), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("placeOfResidence")) {
        sql = appendUpdate(sql, "placeofresidence", connection.escape(user.placeOfResidence), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("maxGraduation")) {
        sql = appendUpdate(sql, "maxgraduation", connection.escape(user.maxGraduation), isFirst);
        isFirst = false;
    }
    if (user.hasOwnProperty("password")) {
        sql = appendUpdate(sql, "password", "'"+user.password+"'", isFirst);
        isFirst = false;
    }
    if (isFirst) {
        throw "no change detected";
    }

    sql += " WHERE `userid` = " + connection.escape(user.authentication.userName) + ";";

    console.log(sql);
    return sql;
}

//Export of the functions defined in this module
module.exports.getUserCount = getUserCount;
module.exports.doesUserExist = doesUserExist;
module.exports.createUser = createUser;
module.exports.checkPassword = checkPassword;
module.exports.updateUser = updateUser;
module.exports.doesEmailExist = doesEmailExist;
module.exports.userInfo = userInfo;