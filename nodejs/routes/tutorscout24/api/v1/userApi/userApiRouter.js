/*Definition of the user api router. This defines the which url/HTTP-Method leads to which module/implementation*/

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var authenticationCheck =require('.././middleware/authenticationCheck');
var ResponseHeader =require('.././middleware/response_header');

router
    //Use the body parser middleware to parse all incoming post requests as json objects.
    .use(bodyParser.json())
    //Use the a middleware function set the response header.
    .use(ResponseHeader)
    .get("/info",require("./user_info") )
    .post("/userInfo",authenticationCheck, require("./user_userinfo"))
    .post("/create",require("./user_create"))
    .post("/checkAuthentication",authenticationCheck, require("./user_checkAuthentication"))
    .post("/completeRegistration", function (req, res, next) {      //Double Opt-in
        res.status(200).send('Email check test response')
    })
    .put("/updateUser",authenticationCheck,require("./user_updateUser"))
    .post("/myUserInfo",authenticationCheck, require("./user_myUserInfo"));

module.exports = router;