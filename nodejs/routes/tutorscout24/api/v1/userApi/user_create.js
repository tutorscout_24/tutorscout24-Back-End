var User = require('../request_classes/userCreateRequest');
var validate = require('jsonschema').validate;
var userDb = require('../database/users_database');

module.exports =  function (req, res, next) {
    var validation = validate(req.body, User.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }

    //Birth date string validation
    //Check if the year is bigger than the current one and if it is more than 100 years ago
    if(Number(req.body.birthdate.substr(0,4)) >= (new Date()).getFullYear() || Number(req.body.birthdate.substr(0,4)) < (new Date()).getFullYear() - 100){
        res.status(400).json("Birthdate: Year is bigger then current Year or age is bigger then 100");
        return;
    }
    //Check if month is bigger 12 or lower 0
    if(Number(req.body.birthdate.substr(4,2))-1 > 12 || Number(req.body.birthdate.substr(4,2))-1 < 0){
        res.status(400).json("Birthdate: Month is bigger then 12 or lower zero");
        return;
    }
    //Check if the days fits with the given month
    if(Number(req.body.birthdate.substr(4,2)) == 01 | 03 | 05 | 07 | 08 | 10 | 12 && Number(req.body.birthdate.substr(6,2) > 31 ||Number(req.body.birthdate.substr(6,2) <= 0 ))){
        res.status(400).json("This month has 31 days");
        return;
    }
    //Check if the days fits with the given month
    if(Number(req.body.birthdate.substr(4,2)) == 04 | 06 | 09 | 11  && Number(req.body.birthdate.substr(6,2) > 30 ||Number(req.body.birthdate.substr(6,2) <= 0 ))){
        res.status(400).json("This month has 30 days");
        return;
    }
    //Check if the days fits with the given month
    if(Number(req.body.birthdate.substr(4,2)) == 02  && Number(req.body.birthdate.substr(6,2) > 28 ||Number(req.body.birthdate.substr(6,2) <= 0 ))){
        res.status(400).json("This month has 28 days");
        return;
    }


    //Check if a user with the given userid does allready exist
    userDb.doesUserExist(req.userName, function (err, userExists) {
        if (err) {
            res.status(500).json("Error during database query");
            return;
        }

        if (userExists) {
            res.status(500).json("User already exists");
            return;
        }

        //Create the user in the database
        userDb.createUser(req.body, function (error) {
            if (error) {

                if(error.code =="ER_DUP_ENTRY")
                {
                    res.status(500).json("The userid or mail is already in use");
                    return;
                }

                res.status(500).json("Error during database query");
                console.log(error.message);
                return;
            }

            res.status(200).json('User successfully created')
        })
    })
};