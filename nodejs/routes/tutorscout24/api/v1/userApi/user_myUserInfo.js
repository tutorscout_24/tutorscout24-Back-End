var validate = require('jsonschema').validate;
var userInfo = require('../request_classes/userInfoRequest');
var userDb = require('../database/users_database');
var MyUserInfo = require('.././data_classes/myUserInfo');

module.exports = function (req, res, next) {
    userDb.userInfo(req.body.authentication.userName, function (error, user) {
        if (error != false) {
            res.status(500).json("Error during Database query");
            console.error(error);
            return;
        }
        else {
            var useInf = new MyUserInfo.MyUserInfo(user[0].userid, user[0].firstname, user[0].lastname, user[0].birthdate, user[0].gender, user[0].description, user[0].maxgraduation,user[0].email,user[0].placeofresidence);
            res.status(200).send(JSON.stringify(useInf, null, 4));
        }
    });
};