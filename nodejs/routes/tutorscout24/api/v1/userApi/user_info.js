var Info = require('.././data_classes/info');
var userDb = require('.././database/users_database');

module.exports =function (req, res, next) {
    userDb.getUserCount(function (error, userCount) {
        if (error) {
            res.status(500).json("Error during database query");
            return;
        }
        else {
            var acInfo = new Info.Info(userCount);
            res.status(200).send(JSON.stringify(acInfo, null, 4));
        }
    });
}