var validate = require('jsonschema').validate;
var userInfo = require('../request_classes/userInfoRequest');
var userDb = require('../database/users_database');
var limitedUser = require('.././data_classes/limited_user');

module.exports = function (req, res, next) {
    var validation = validate(req.body, userInfo.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }

    userDb.doesUserExist(req.body.userToFind, function (error, exists) {
        if(error){
            res.status(500).json("Error during Database query");
            return;
        }
        if(!exists){
            res.status(500).json("User does not exist");
            return;
        }
        userDb.userInfo(req.body.userToFind, function (error, user) {
            if (error != false) {
                res.status(500).json("Error during Database query");
                return;
            }
            else {
                var useInf = new limitedUser.LimitedUser(user[0].userid, user[0].firstname, user[0].lastname, getAge(user[0].birthdate), user[0].gender, user[0].description, user[0].maxgraduation);
                res.status(200).send(JSON.stringify(useInf, null, 4));
            }
        })
    });
    function getAge(birthdate) {
        var dob = birthdate;
        var year = Number(dob.substr(0, 4));
        var month = Number(dob.substr(4, 2)) - 1;
        var day = Number(dob.substr(6, 2));
        var today = new Date();
        var age = today.getFullYear() - year;
        if (today.getMonth() < month || (today.getMonth() == month && today.getDate() < day)) {
            age--;
        }
        return age;

    }
};