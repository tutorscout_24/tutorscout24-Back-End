var validate = require('jsonschema').validate;
var UserUpdate = require('../request_classes/userUpdateRequest');
var userDb = require('../database/users_database');

module.exports = function (req, res, next) {
    var validation = validate(req.body, UserUpdate.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }
    if('birthdate' in req.body) {
        if (Number(req.body.birthdate.substr(0, 4)) >= (new Date()).getFullYear() || Number(req.body.birthdate.substr(0, 4)) < 0) {
            res.status(400).json("Birthdate: Year is bigger then current Year or lower zero");
            return;
        }
        if (Number(req.body.birthdate.substr(4, 2)) - 1 > 12 || Number(req.body.birthdate.substr(4, 2)) - 1 < 0) {
            res.status(400).json("Birthdate: Month is bigger then 12 or lower zero");
            return;
        }
        if (Number(req.body.birthdate.substr(4, 2)) == 01 | 03 | 05 | 07 | 08 | 10 | 12 && Number(req.body.birthdate.substr(6, 2) > 31 || Number(req.body.birthdate.substr(6, 2) <= 0))) {
            res.status(400).json("This month has 31 days");
            return;
        }
        if (Number(req.body.birthdate.substr(4, 2)) == 04 | 06 | 09 | 11 && Number(req.body.birthdate.substr(6, 2) > 30 || Number(req.body.birthdate.substr(6, 2) <= 0))) {
            res.status(400).json("This month has 30 days");
            return;
        }
        if (Number(req.body.birthdate.substr(4, 2)) == 02 && Number(req.body.birthdate.substr(6, 2) > 28 || Number(req.body.birthdate.substr(6, 2) <= 0))) {
            res.status(400).json("This month has 28 days");
            return;
        }

    }

    if ('email' in req.body) {
        userDb.doesEmailExist(req.body.email, function (error,exists) {
            if (error) {
                res.status(500).json("Error during email check database query");
                return;
            }
            if (exists) {
                res.status(500).json("The given mail is already in use");
                return;
            }

            userDb.updateUser(req.body, function (error) {
                if (error) {
                    res.status(500).json("Error during database query");
                    console.log(error);
                    return;
                }
                else {
                    res.status(200).json('User data successfully changed');
                    return;
                }
            })
        });
    }
    else {
        userDb.updateUser(req.body, function (error) {
            if (error) {
                res.status(500).json("Error during database query");
                return;
            }
            else {
                res.status(200).json('User data successfully changed');
                return;
            }
        })
    }
}