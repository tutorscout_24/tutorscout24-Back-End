var messageGet = require('../request_classes/messageGetRequest');
var Message = require('.././data_classes/message');
var validate = require('jsonschema').validate;
var messageDb = require('.././database/message_database');

module.exports = function (req, res, next) {

    //Database call to get all unread, received messages
    messageDb.getUnreadMessage(req.body.authentication.userName, function (error, result) {
        if (error) {
            res.status(500).json("Error during Database query");
        }
        else {
            messageDb.messageIsRead(req.body.authentication.userName, function (error, result) {
                if (error) {
                    res.status(500).json("Error during Database query");
                }
            });
            if (result.length == 0) {
                //result Array is empty = no messages
                res.status(409).json("No unread Messages ");
                return;
            }
            else {
                res.status(200).send(JSON.stringify(result, null, 4));
            }
        }
    });
};