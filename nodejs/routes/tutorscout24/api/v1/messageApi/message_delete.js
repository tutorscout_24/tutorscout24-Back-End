var messageDelete = require('../request_classes/messageDeleteRequest');
var validate = require('jsonschema').validate;
var messageDb = require('.././database/message_database');

module.exports = function (req, res, next) {

    //Validation with the Schema from messageDeleteRequest
    var validation = validate(req.body, messageDelete.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }

    //Database call to check whether user is sender or receiver
    messageDb.isUserSenderOrReceiver(req.body, function (error, result) {
        if (error) {
            res.status(500).json("Error during Database query");
        }
        else {
            if (result == "sender") {
                //Database call in case of user is sender
                messageDb.deleteBySender(req.body.messageId, function (error, result) {
                    if (error) {
                        res.status(500).json("Error during Database query");
                    }
                    else {
                        //Database call to remove all messages deleted by sender and receiver
                        messageDb.removeMessage(function (error, result) {
                            if (error) {
                                res.status(500).json("Error during Database query");
                            }
                            else {
                                res.status(200).json("Message successfully deleted");
                            }
                        })
                    }
                })
            }
            if (result == "receiver") {
                //Database call in case of user is receiver
                messageDb.deleteByReceiver(req.body.messageId, function (error, result) {
                    if (error) {
                        res.status(500).json("Error during Database query");
                    }
                    else {
                        //Database call to remove all messages deleted by sender and receiver
                        messageDb.removeMessage(function (error, result) {
                            if (error) {
                                res.status(500).json("Error during Database query");
                            }
                            else
                                {
                                    res.status(200).json("Message successfully deleted");
                                }
                            }
                        )
                    }
                })
            }
        }
    });
};