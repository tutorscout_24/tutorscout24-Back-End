var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var authenticationCheck =require('.././middleware/authenticationCheck');
var ResponseHeader =require('.././middleware/response_header');



router
//Use the body parser middleware to parse all incoming post requests as json objects.
    .use(bodyParser.json())
    //Use the a middleware function set the response header.
    .use(ResponseHeader)
    .post("/sendMessage",authenticationCheck, require("./message_send"))
    .delete("/deleteMessage",authenticationCheck, require("./message_delete"))
    .post("/getSentMessages",authenticationCheck, require("./message_getSent"))
    .post("/getReceivedMessages",authenticationCheck, require("./message_getReceived"))
    .post("/getUnreadMessages", authenticationCheck, require("./message_getUnread"));

module.exports = router;