var message = require('../request_classes/messageSendRequest');
var validate = require('jsonschema').validate;
var messageDb = require('.././database/message_database');
var userDb = require('../database/users_database');

module.exports = function (req, res, next) {

    var validation = validate(req.body, message.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }

    if(req.body.toUserId== req.body.authentication.userName)
    {
        res.status(400).json("The sender can not also be the receiver.");
        return;
    }

    //Database call to check if receiver exists
    userDb.doesUserExist(req.body.toUserId, function (error, exists) {
        if (error) {
            res.status(500).json("Error during Database query");
            return;
        }
        if (!exists) {
            res.status(400).json("Receiver does not exist");
            return;
        }
        //Database call to store message
        messageDb.storeMessage(req.body, function (error) {
            if (error) {
                res.status(500).json("Error during database query");
                console.log(error.message);
                return;
            }
            res.status(200).json('Message successfully send')
        })
    });

};