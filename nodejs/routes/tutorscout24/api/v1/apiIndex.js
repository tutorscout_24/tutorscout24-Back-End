var express = require('express');
var router = express.Router();
var HttpStatus = require('http-status-codes');

/* GET API V1 Index page. */
router
    .get('/', function (req, res, next) {
        //Render the api index page template into the response.
        res.status(HttpStatus.OK).render('api/apiIndex', {version: 'V1'});
    });

module.exports = router;