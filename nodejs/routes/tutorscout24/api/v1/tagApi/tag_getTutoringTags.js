var TagGetRequest = require('.././request_classes/tagGetTutoringTagsRequest');
var validate = require('jsonschema').validate;
var tagDb = require('../database/tag_database');


module.exports =  function (req, res, next) {
    var validation = validate(req.body, TagGetRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }
    tagDb.getTutoringTags(req.body.tutoringId,function (error,results) {
        if (error) {
            res.status(500).json("Error during database query. Could not find tags for given tutoring id "+error);
            return;
        }
        else {
            res.status(200).send(JSON.stringify(results, null, 4));
            return;
        }
    })
};