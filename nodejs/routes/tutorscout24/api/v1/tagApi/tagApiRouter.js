/*Definition of the 'tag' api router. This defines the which url/HTTP-Method leads to which module/implementation*/

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var authenticationCheck =require('.././middleware/authenticationCheck');
var ResponseHeader =require('.././middleware/response_header');



router
//Use the body parser middleware to parse all incoming post requests as json objects.
    .use(bodyParser.json())
    //Use the a middleware function set the response header.
    .use(ResponseHeader)
    .post("/create",authenticationCheck,require("./tag_create"))
    .post("/getTutoringTags",authenticationCheck,require("./tag_getTutoringTags"))
    .delete("/delete",authenticationCheck,require("./tag_delete"))

module.exports = router;