var TagCreateRequest = require('.././request_classes/tagCreateRequest');
var validate = require('jsonschema').validate;
var tagDb = require('../database/tag_database');
var tutoringDb = require('../database/tutoring_database');


module.exports =  function (req, res, next) {
    var validation = validate(req.body, TagCreateRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }
    tutoringDb.hasUserTutoring(req.body.authentication.userName,req.body.tutoringId,function (error,hasTutoring) {
        if (error) {
            res.status(500).json("Error during database query. Search for user tutoring failed.");
            return;
        }
        if(!hasTutoring)
        {
            res.status(400).json("The given user does not have a tutoring with the given id");
            return;
        }
        tagDb.createTutoringTag(req.body.tutoringId,req.body.tagText,function (error) {
            if (error) {
                res.status(500).json("Error during database query. Creation of tag failed."+error);
                return;
            }

            res.status(200).json("Tutoring tag successfully created");
        });
    });
};