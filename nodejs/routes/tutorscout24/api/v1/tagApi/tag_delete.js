var TagDeleteRequest = require('.././request_classes/tagDeleteRequest');
var validate = require('jsonschema').validate;
var tagDb = require('../database/tag_database');


module.exports =  function (req, res, next) {
    var validation = validate(req.body, TagDeleteRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }
    tagDb.deleteTutoringTag(req.body.tagId,function (error,successfull) {
        if (error) {
            res.status(500).json("Error during database query. Could not delete the tutoring tag :"+error);
            return;
        }
        if(!successfull)
        {
            res.status(400).json("No tags were deleted");
        }
        else
        {
            res.status(200).json("Tag successfully deleted");
        }
    })
};