var Tutoring = require('../request_classes/tutoringCreateRequest');
var validate = require('jsonschema').validate;
var TutoringDb = require('../database/tutoring_database');



module.exports =  function (req, res, next) {
    var validation = validate(req.body, Tutoring.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }

    TutoringDb.createTutoringOffer(req.body.authentication.userName,req.body, function (error) {
        if (error) {
            res.status(500).json("Error during database query");
            return;
        }
        else {
            res.status(200).json('Tutoring request successfully created.');
            return;
        }
    });
};