var TutoringDb = require('../database/tutoring_database');
var validate = require('jsonschema').validate;
var TutoringDeleteRequest = require('../request_classes/tutoringDeleteRequest');

module.exports = function (req, res, next) {

    //Deni the request if the posted data does not match the schema
    var validation = validate(req.body, TutoringDeleteRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }


    TutoringDb.hasUserTutoring(req.body.authentication.userName, req.body.tutoringId, function (error, hasTutoring) {
        if (error) {
            res.status(500).json("Error during database query");
            console.error(error);
            return;
        }
        else if (!hasTutoring) {
            res.status(400).json("The current user does not have a tutoring with this id");
            return;
        }

        TutoringDb.deleteTutoring(req.body.tutoringId, function (error) {
            if (error) {
                res.status(500).json("Error during database query(could not delete tutoring)");
                console.error(error);
                return;
            }
            res.status(200).json("Tutoring successfully deleted");
        });
    });
};