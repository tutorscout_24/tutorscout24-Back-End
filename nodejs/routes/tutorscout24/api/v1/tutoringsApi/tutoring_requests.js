var TutoringDb = require('../database/tutoring_database');
var TutoringFindRequest = require('../request_classes/tutoringFindRequest');
var validate = require('jsonschema').validate;

module.exports =  function (req, res, next) {

    //Deni the request if the posted data does not match the schema
    var validation = validate(req.body, TutoringFindRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }

    TutoringDb.getTutoringRequestsWithinRange(req.body.latitude,req.body.longitude,req.body.rangeKm,req.body.rowLimit,req.body.rowOffset,req.body.authentication.userName, function (error,results) {
        if (error) {
            res.status(500).json("Error during database query:"+error);
            return;
        }
        else {
            res.status(200).send(JSON.stringify(results, null, 4));
            return;
        }
    });
};