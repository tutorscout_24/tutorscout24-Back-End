var TutoringDb = require('../database/tutoring_database');

module.exports =  function (req, res, next) {
    TutoringDb.getUserTutoringsRequests(req.body.authentication.userName, function (error,results) {
        if (error) {
            res.status(500).json("Error during database query");
            return;
        }
        else {
            res.status(200).send(JSON.stringify(results,null,4));
            return;
        }
    });
};