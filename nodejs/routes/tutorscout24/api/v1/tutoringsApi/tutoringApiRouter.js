/*Definition of the tutoring api router. This defines the which url/HTTP-Method leads to which module/implementation*/

var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
var authenticationCheck =require('.././middleware/authenticationCheck');
var ResponseHeader =require('.././middleware/response_header');



router
//Use the body parser middleware to parse all incoming post requests as json objects.
    .use(bodyParser.json())
    //Use the a middleware function set the response header.
    .use(ResponseHeader)
    .post("/createOffer",authenticationCheck,require("./tutoring_createOffer"))
    .post("/createRequest",authenticationCheck,require("./tutoring_createRequest"))
    .delete("/delete",authenticationCheck,require("./tutoring_delete"))
    .post("/myRequests",authenticationCheck,require("./tutoring_myRequests"))
    .post("/myOffers",authenticationCheck,require("./tutoring_myOffers"))
    .post("/requests",authenticationCheck,require("./tutoring_requests"))
    .post("/offers",authenticationCheck,require("./tutoring_offers"))

module.exports = router;