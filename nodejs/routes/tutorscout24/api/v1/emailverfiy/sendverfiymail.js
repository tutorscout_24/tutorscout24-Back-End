var randomstring = require("randomstring");
var smtp =require('./smtptransport');
var con =require('.././database/database_connection');
var conn = con.Connection;


var smtpTransport = smtp.smtptransport();

var rand,mailOptions,host,link;

// Die Funktionen ist Asynchrone aufgebaut da so eine schnellere bearbeitung dieses Prozesses stattfinden kann da nicht auf den Datenbankzugriff gewartet werden muss
// sondern direkt die Mail versendet wird beim fehlschlagen von einer Operation siehe sendnewverfiymail

function sendMail(user,res){

    // Generien des zufallstring als Code zum überprüfen und Angabe des host sowie infos zu code und user im Link der in der Email verschickt wird

	 rand=randomstring.generate(10);
	 host = 'http://localhost:3000/tutorscout24/api/v1/emailverify/completeRegistration'
	 link=host+'?id='+rand+'&email='+user.email+'&name='+user.firstName;

	 // Den Code in die Datenbank schreiben um in später überprüfen zu können

    var sql = "UPDATE user SET emailverfiytoken='"+rand+"' WHERE email ='"+user.email+"';";
    conn.query(sql, function (err, result) {
        if (err){
            throw err;

        }
        console.log("Record inserted");

    });

    // Asynchroner Aufruf zum versenden der Mail mit dem Link zum anklicken

	 mailOptions={
		        to : user.email,
		        subject : "Please confirm your email account",
         html : 'Hello '+user.firstName+',<br> Please Click on the link to verify your email.<br><br><a href='+link+'>Click here to verify</a> <br><br> If you have not registered at Tutorscout24 you can ignore this. '
     };
	 
	 
	 smtpTransport.sendMail(mailOptions, function(error, info){
	     if(error){
	     	console.log(error);
             return res(error);
	     }else{
	                   
	            console.log("Message sent: " + info.response);
            	 res(null);
	            	        
	         }
	});
	
	 
};

module.exports.sendMail= sendMail;

