var PossibleTimeDeleteRequest = require('.././request_classes/possibleTimeDeleteRequest');
var validate = require('jsonschema').validate;
var PossibleTimesDb = require('../database/possibleTime_database');



module.exports =  function (req, res, next) {
    var validation = validate(req.body, PossibleTimeDeleteRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }
    PossibleTimesDb.deletePossibleTimes(req.body.timeId,req.body.authentication.userName,function (error,deleted) {
        if (error) {
            res.status(500).json("Error during database query");
            console.error(error);
            return;
        }

        if (!deleted) {
            res.status(400).json("Request did not affect any time entry");
            return;
        }

        else {
            res.status(200).json("Successfully deleted time entry");
            return;
        }
    })
};