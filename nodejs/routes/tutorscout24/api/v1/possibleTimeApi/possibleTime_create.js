var PossibleTimeCreateRequest = require('.././request_classes/possibleTimeCreateRequest');
var validate = require('jsonschema').validate;
var TutoringDb = require('../database/tutoring_database');
var PossibleTimesDb = require('../database/possibleTime_database');



module.exports =  function (req, res, next) {
    var validation = validate(req.body, PossibleTimeCreateRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "["+error.property+"]"+error.message;
        }), null, 4));
        return;
    }
    TutoringDb.hasUserTutoring(req.body.authentication.userName,req.body.tutoringId,function (error,hasTutoring) {
        if (error) {
            res.status(500).json("Error during database query. Search for user tutoring failed.");
            return;
        }
        if(!hasTutoring)
        {
            res.status(400).json("The given user does not have a tutoring with the given id");
            return;
        }
        PossibleTimesDb.createPossibleTime(req.body.tutoringId,req.body.authentication.userName,req.body.start,req.body.end,function (error) {
            if (error) {
                res.status(500).json("Error during database query, Possible Time not created");
                return;
            }

            res.status(200).json("Time entry successfully created");
            return;
        })
    })
};