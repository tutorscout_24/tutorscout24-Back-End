var PossibleTimeGetRequest = require('.././request_classes/possibleTimeGetRequest');
var validate = require('jsonschema').validate;
var PossibleTimesDb = require('../database/possibleTime_database');



module.exports =  function (req, res, next) {
    var validation = validate(req.body, PossibleTimeGetRequest.Schema);
    if (!validation.valid) {
        res.status(400).send(JSON.stringify(validation.errors.map(function (error) {
            return "[" + error.property + "]" + error.message;
        }), null, 4));
        return;
    }
    PossibleTimesDb.getPossibleTimes(req.body.tutoringId,function (error,results) {
        if (error) {
            res.status(500).json("Error during database query");
            return;
        }
        else {
            res.status(200).send(JSON.stringify(results, null, 4));
            return;
        }
    })
};