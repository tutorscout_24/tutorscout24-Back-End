swagger: '2.0'
info:
  description: Tutorscout24 Back-End API
  version: 1.0.0
  title: Tutorscout24
host: 'tutorscout24.vogel.codes:3000'
basePath: /tutorscout24/api/v1/
tags:
  - name: user
    description: All user related operations
schemes:
  - http
paths:
  /user/info:
    get:
      tags:
        - user
      summary: Provides information about count of registerd users
      description: ''
      operationId: Info
      consumes:
        - application/json
      produces:
        - application/json
      responses:
        '200':
          description: Information about the registered users will be send.
          schema:
            $ref: '#/definitions/Info'
        '500':
          description: Error during database query
  /user/create:
    post:
      tags:
        - user
      summary: Creates a new user
      description: ''
      operationId: createUser
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: User object that contains all required data for creating a new user.
          required: true
          schema:
            $ref: '#/definitions/User'
      responses:
        '200':
          description: User succesfully created
        '400':
          description: Invalid Json data
  /user/checkAuthentication:
    post:
      tags:
        - user
      summary: 'Checks the Authentication(userid,password) of a user'
      description: ''
      operationId: checkAuthentication
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Authentication
          required: true
          schema:
            $ref: '#/definitions/CheckAuthenticationRequest'
      responses:
        '200':
          description: Authentication successfull
        '204':
          description: Request successfull but invalid authentification data.
        '400':
          description: Invalid Json data
  /user/userInfo:
    post:
      tags:
        - user
      summary: Provides User information
      description: ''
      operationId: userInfo
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Authentification and Id of the requested data.
          required: true
          schema:
            $ref: '#/definitions/UserInfoRequest'
      responses:
        '200':
          description: Account Information will be send.
          schema:
            $ref: '#/definitions/userInfo'
        '204':
          description: Request successfull but no user with this name
        '400':
          description: Invalid Json data
        '403':
          description: No permission to request information (Login)
  /user/completeRegistration:
    post:
      tags:
        - user
      summary: 'During Registration, Email check'
      description: ''
      operationId: completeRegistration
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Sends Email for Double Opt-in
          required: true
          schema:
            $ref: '#/definitions/CompleteRegistration'
      responses:
        '200':
          description: Registration complete
        '204':
          description: Registration already completed
        '400':
          description: Invalid Json data
  /user/updateUser:
    put:
      tags:
        - user
      summary: Update values in User information
      description: ''
      operationId: updateUser
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Authentification
          required: true
          schema:
            $ref: '#/definitions/UserDataUpdate'
      responses:
        '200':
          description: Update successful
        '400':
          description: Invalid Json data
  /user/delete:
    post:
      tags:
        - user
      summary: Deleta a user
      description: ''
      operationId: deleteUser
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Authentication and which user to delete
          required: true
          schema:
            $ref: '#/definitions/CheckAuthenticationRequest'
      responses:
        '200':
          description: Account deleted
        '204':
          description: Request successfull but no user with this name
        '400':
          description: Invalid Json data
  /message/sendMessage:
    post:
      tags:
        - message
      summary: 'Saves Message in Database, receiver has access'
      description: ''
      operationId: sendMessage
      consumes:
        - application/json
      produces:
        - application/json
      parameters:
        - in: body
          name: body
          description: Message string and receiver
          required: true
          schema:
            $ref: '#/definitions/Message'
      responses:
        '200':
          description: Message successfully sent
        '400':
          description: Sender does not exist OR Receiver does not exist
        '500':
          description: Database Error
definitions:
  UserInfoRequest:
    type: object
    properties:
      userToFind:
        type: string
        description: Username of the User that should be searched for
        example: Max123
      authentication:
        $ref: '#/definitions/Id'
  CheckAuthenticationRequest:
    type: object
    properties:
      authentication:
        $ref: '#/definitions/Id'
  Id:
    type: object
    properties:
      userName:
        type: string
        description: Username used in the login process
        example: Max
      password:
        type: string
        description: The password of the new account
        example: Pa$$w0rd
    xml:
      name: Id
  User:
    type: object
    properties:
      userName:
        type: string
        description: user name of the user owner
        example: Max123
      password:
        type: string
        description: The password of the new user
        example: Pa$$w0rd
      firstName:
        type: string
        description: first name of the user owner
        example: Max
      lastName:
        type: string
        description: last name of the user owner
        example: Mustermann
      age:
        type: integer
        description: age of the user owner
        example: 25
      gender:
        type: string
        description: gender of the user owner
        enum:
          - male
          - female
      email:
        type: string
        description: E-Mail adress that belongs to the new user(needs to be verified)
        example: Mustermann@mail.de
      note:
        type: string
        description: user defined profile description
        example: 99 Jahre Berufserfahrung
      placeOfResidence:
        type: string
        description: place where the user lives
        example: Horb
      maxGraduation:
        type: string
        description: highest graduation of the user
        example: Bachelor of Science
    xml:
      name: User
  Info:
    type: object
    properties:
      userCount:
        type: integer
        format: int32
        example: 42
        description: The number of registered users.
  UserDataUpdate:
    type: object
    properties:
      password:
        type: string
        description: The password of the new user
        example: Pa$$w0rd
      firstName:
        type: string
        description: first name of the user owner
        example: Max
      lastName:
        type: string
        description: last name of the user owner
        example: Mustermann
      age:
        type: integer
        description: age of the user owner
        example: 25
      gender:
        type: string
        description: gender of the user owner
        enum:
          - male
          - female
      email:
        type: string
        description: E-Mail adress that belongs to the new user(needs to be verified)
        example: Mustermann@mail.de
      note:
        type: string
        description: user defined profile description
        example: 99 Jahre Berufserfahrung
      placeOfResidence:
        type: string
        description: place where the user lives
        example: Horb
      maxGraduation:
        type: string
        description: highest graduation of the user
        example: Bachelor of Science
      authentication:
        $ref: '#/definitions/Id'
    xml:
      name: User
  userInfo:
    type: object
    properties:
      username:
        type: string
        description: Username used in the login process
        example: Max
      firstName:
        type: string
        description: first name of the user owner
        example: Max
      lastName:
        type: string
        description: last name of the user owner
        example: Mustermann
      age:
        type: integer
        description: age of the user owner
        example: '25'
      gender:
        type: string
        description: gender of the user owner
        enum:
          - male
          - female
      placeOfResidence:
        type: string
        description: place where the user lives
        example: Horb
      note:
        type: string
        description: user defined profile description
        example: 99 Jahre Berufserfahrung
    xml:
      name: singleUserInfo
  Message:
    type: object
    properties:
      fromUserId:
        type: string
        description: Message to another User
        example: 'Hi, what''s up?'
      toUserId:
        type: string
        description: Receiver of the message
        example: tutor123
      text:
        type: string
        description: Message to another User
        example: 'Hi, what''s up?'
    xml:
      name: Message
  CompleteRegistration:
    type: object
    properties:
      registrationCode:
        type: string
        description: Code for Email authentification
        example: 12345
