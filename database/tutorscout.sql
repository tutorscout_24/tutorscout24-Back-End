-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema tutorscout
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema tutorscout
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `tutorscout` DEFAULT CHARACTER SET utf8 ;
USE `tutorscout` ;

-- -----------------------------------------------------
-- Table `tutorscout`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`user` (
  `userid` VARCHAR(45) NOT NULL,
  `firstname` VARCHAR(45) NOT NULL,
  `lastname` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(60) NOT NULL,
  `placeofresidence` VARCHAR(45) NULL DEFAULT NULL,
  `birthdate` VARCHAR(8) NULL DEFAULT NULL,
  `gender` VARCHAR(45) NULL DEFAULT NULL,
  `maxgraduation` VARCHAR(45) NULL DEFAULT NULL,
  `description` VARCHAR(45) NULL DEFAULT NULL,
  `creationdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emailVerified` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`userid`),
  UNIQUE INDEX `userid_UNIQUE` (`userid` ASC),
  UNIQUE INDEX `e-mail_UNIQUE` (`email` ASC))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`image`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`image` (
  `imageid` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NULL DEFAULT NULL,
  `comment` VARCHAR(45) NULL DEFAULT NULL,
  `image` VARCHAR(45) NULL DEFAULT NULL,
  `User_userid` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`imageid`),
  INDEX `fk_Image_User_idx` (`User_userid` ASC),
  UNIQUE INDEX `imageid_UNIQUE` (`imageid` ASC),
  CONSTRAINT `fk_Image_User`
    FOREIGN KEY (`User_userid`)
    REFERENCES `tutorscout`.`user` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`message`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`message` (
  `messageid` INT(11) NOT NULL AUTO_INCREMENT,
  `datetime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `unread` TINYINT NULL DEFAULT NULL,
  `fromuserid` VARCHAR(45) NOT NULL,
  `touserid` VARCHAR(45) NOT NULL,
  `text` VARCHAR(500) NULL DEFAULT NULL,
  `deletedbysender` TINYINT NOT NULL DEFAULT 0,
  `deletedbyreceiver` TINYINT NOT NULL DEFAULT 0,
  PRIMARY KEY (`messageid`),
  INDEX `userid_idx` (`fromuserid` ASC),
  INDEX `touserid_idx` (`touserid` ASC),
  UNIQUE INDEX `messageid_UNIQUE` (`messageid` ASC),
  CONSTRAINT `fromuserid`
    FOREIGN KEY (`fromuserid`)
    REFERENCES `tutorscout`.`user` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `touserid`
    FOREIGN KEY (`touserid`)
    REFERENCES `tutorscout`.`user` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`tutoring`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`tutoring` (
  `tutoringid` INT(11) NOT NULL AUTO_INCREMENT,
  `creationdate` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createruserid` VARCHAR(45) NOT NULL,
  `subject` VARCHAR(45) NULL DEFAULT NULL,
  `text` VARCHAR(1000) NULL DEFAULT NULL,
  `type` VARCHAR(45) NULL DEFAULT NULL,
  `end` DATETIME NULL,
  `latitude` DOUBLE NOT NULL,
  `longitude` DOUBLE NOT NULL,
  PRIMARY KEY (`tutoringid`),
  UNIQUE INDEX `tutoringid_UNIQUE` (`tutoringid` ASC),
  INDEX `userid_idx` (`createruserid` ASC),
  CONSTRAINT `userid`
    FOREIGN KEY (`createruserid`)
    REFERENCES `tutorscout`.`user` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`possibletime`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`possibletime` (
  `timeid` INT(11) NOT NULL AUTO_INCREMENT,
  `start` DATETIME NOT NULL,
  `end` DATETIME NOT NULL,
  `User_userid` VARCHAR(45) NOT NULL,
  `tutoringid` INT NOT NULL,
  PRIMARY KEY (`timeid`),
  INDEX `fk_PossibleTime_User1_idx` (`User_userid` ASC),
  UNIQUE INDEX `timeid_UNIQUE` (`timeid` ASC),
  INDEX `fk_tutoring_tutoringid_idx` (`tutoringid` ASC),
  CONSTRAINT `fk_PossibleTime_User1`
    FOREIGN KEY (`User_userid`)
    REFERENCES `tutorscout`.`user` (`userid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_tutoring_tutoringid`
    FOREIGN KEY (`tutoringid`)
    REFERENCES `tutorscout`.`tutoring` (`tutoringid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`tutoringimages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`tutoringimages` (
  `tutoringimageid` INT(11) NOT NULL AUTO_INCREMENT,
  `Image_imageid` INT(11) NOT NULL,
  `Tutoring_tutoringid` INT(11) NOT NULL,
  PRIMARY KEY (`tutoringimageid`),
  INDEX `fk_TutoringImages_Image1_idx` (`Image_imageid` ASC),
  INDEX `fk_TutoringImages_Tutoring1_idx` (`Tutoring_tutoringid` ASC),
  UNIQUE INDEX `tutoringimageid_UNIQUE` (`tutoringimageid` ASC),
  CONSTRAINT `fk_TutoringImages_Image1`
    FOREIGN KEY (`Image_imageid`)
    REFERENCES `tutorscout`.`image` (`imageid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_TutoringImages_Tutoring1`
    FOREIGN KEY (`Tutoring_tutoringid`)
    REFERENCES `tutorscout`.`tutoring` (`tutoringid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `tutorscout`.`tutoringtag`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `tutorscout`.`tutoringtag` (
  `tagid` INT NOT NULL AUTO_INCREMENT,
  `tutoringid` INT NOT NULL,
  `text` VARCHAR(20) NOT NULL,
  PRIMARY KEY (`tagid`),
  INDEX `tutoringid_idx` (`tutoringid` ASC),
  CONSTRAINT `tutoringid`
    FOREIGN KEY (`tutoringid`)
    REFERENCES `tutorscout`.`tutoring` (`tutoringid`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
