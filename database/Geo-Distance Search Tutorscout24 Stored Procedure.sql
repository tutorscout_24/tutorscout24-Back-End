USE tutorscout;
DROP PROCEDURE IF EXISTS geodistTutoringOffersFromOrigin;
DROP PROCEDURE IF EXISTS geodistTutoringReuquestsFromOrigin;

DELIMITER #
CREATE PROCEDURE geodistTutoringOffersFromOrigin (IN centerLatitude double,IN centerLongitude double, IN distKm int,IN rowLimit int,rowOffset int,userId varchar(45))
BEGIN


declare milesToKm double;
declare dist double;
declare lon1 double;
declare lon2 double;
declare lat1 double;
declare lat2 double;

-- Constant for converting from miles to km
set milesToKm = 1.609344;

-- calculate lon and lat for the rectangle
set dist= distKm/milesToKm;
set lon1 = centerLongitude - dist/abs(cos(radians(centerLatitude))*69);
set lon2 = centerLongitude + dist/abs(cos(radians(centerLatitude))*69);
set lat1 = centerLatitude - (dist/69);
set lat2 = centerLatitude + (dist/69);

-- Execute the quary
SELECT 
  tutoringid as tutoringId,creationdate as creationDate,createruserid as userName,subject,text,end as expirationDate,latitude,longitude
  ,((3956 * 2 * ASIN(SQRT(POWER(SIN((centerLatitude - tutoring.latitude) * PI() / 180 / 2),2) + COS(centerLatitude * PI() / 180) * COS(tutoring.latitude * PI() / 180) * POWER(SIN((centerLongitude - tutoring.longitude) * PI() / 180 / 2),2))))* milesToKm) AS distanceKm
FROM
    tutorscout.tutoring
WHERE
    tutoring.longitude BETWEEN lon1 AND lon2
    AND tutoring.latitude BETWEEN lat1 AND lat2
    AND tutoring.type='offer'
	AND tutoring.createruserid <> userId
HAVING distanceKm < distKm
LIMIT rowLimit OFFSET rowOffset;
END #
DELIMITER ;


DELIMITER #
CREATE PROCEDURE geodistTutoringRequestsFromOrigin (IN centerLatitude double,IN centerLongitude double, IN distKm int,IN rowLimit int,rowOffset int,userId varchar(45))
BEGIN


declare milesToKm double;
declare dist double;
declare lon1 double;
declare lon2 double;
declare lat1 double;
declare lat2 double;

-- Constant for converting from miles to km
set milesToKm = 1.609344;

-- calculate lon and lat for the rectangle
set dist= distKm/milesToKm;
set lon1 = centerLongitude - dist/abs(cos(radians(centerLatitude))*69);
set lon2 = centerLongitude + dist/abs(cos(radians(centerLatitude))*69);
set lat1 = centerLatitude - (dist/69);
set lat2 = centerLatitude + (dist/69);

-- Execute the quary
SELECT 
   tutoringid as tutoringId,creationdate as creationDate,createruserid as userName,subject,text,end as expirationDate,latitude,longitude
   ,((3956 * 2 * ASIN(SQRT(POWER(SIN((centerLatitude - tutoring.latitude) * PI() / 180 / 2),2) + COS(centerLatitude * PI() / 180) * COS(tutoring.latitude * PI() / 180) * POWER(SIN((centerLongitude - tutoring.longitude) * PI() / 180 / 2),2))))* milesToKm) AS distanceKm
FROM
    tutorscout.tutoring
WHERE
    tutoring.longitude BETWEEN lon1 AND lon2
    AND tutoring.latitude BETWEEN lat1 AND lat2
    AND tutoring.type='request'
	AND tutoring.createruserid <> userId
HAVING distanceKm < distKm
LIMIT rowLimit OFFSET rowOffset;
END #
DELIMITER ;